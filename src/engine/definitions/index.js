export default [
  require('./look'),
  require('./lookDirection'),
  require('./lookAt'),
  require('./go'),
  require('./open'),
  // require('./unlockDoor'),
  require('./take-drop'),
  require('./inventory'),
  require('./help'),
  require('./options'),
  require('./map'),
  require('./hint'),
  require('./start'),
  require('./take-apart')
]
